import java.util.Arrays;

public class fibonacciRecursao {

	public static void main(String[] args){
		System.out.println(fibonacci(7));
	}

	public static int fibonacci(int index) {
		if(index == 0 || index == 1){
			return 1;
		}
		else {
			return fibonacci(index - 1) + fibonacci(index - 2);
		}
	}
}
