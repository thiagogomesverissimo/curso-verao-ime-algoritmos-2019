
public class fibonacciIterative {

	public static void main(String[] args){
		System.out.println(fibonacci(70));
	}

	public static int fibonacci(int index) {
		int penultimo = 1;
		int ultimo = 1;

		for(int i = 2; i <= index; i++ ) {
			// esse bloco pode ser substituido pelo de baixo
			//int atual = ultimo + penultimo;
			//penultimo = ultimo;
			//ultimo = atual;

			// novo jeito
			ultimo = penultimo + ultimo;
			penultimo = ultimo - penultimo;
		}
		return ultimo;
	}
}
