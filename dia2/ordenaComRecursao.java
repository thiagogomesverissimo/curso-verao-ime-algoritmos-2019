import java.util.Arrays;

public class ordenaComRecursao {

	public static void main(String[] args){
		double[] numeros = new double[] {10.,2.,5.,6.,8.,3.,1.0,4.,0.};
		System.out.println(Arrays.toString(ordenaPorSelecaoRecursao(numeros, 0, numeros.length - 1 )));
	}

	public static double[] ordenaPorSelecaoRecursao(double[] numeros, int inicio, int fim) {

		if(inicio < fim) {
			int maior = achaPosicaoDoMaior(numeros,inicio,fim);
			double valor_maior = numeros[maior];
			double valor_fim = numeros[fim];

			numeros[fim] = valor_maior;
			numeros[maior] = valor_fim;
			return ordenaPorSelecaoRecursao(numeros,inicio,fim-1);
		} else {
			return numeros;
		}
	
	}

	public static int achaPosicaoDoMaior(double[] numeros, int inicio, int fim) {

		int maior = inicio;
		for (int i = inicio + 1; i <= fim; i++) {
			if( numeros[i] > numeros[maior] ) {
				maior = i;
			}
		}
		return maior;
	}
}
