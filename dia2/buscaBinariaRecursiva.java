import java.util.Arrays;

public class buscaBinariaRecursiva {

	public static void main(String[] args){
		// ATENÇÃO: BUSCA PARA NUMEROS ORDENADOS!!!!!!!!!!!!
		int[] numeros = new int[] {1, 3, 4, 20, 23, 48, 67, 100, 777, 10000};
		System.out.println(busca(numeros, 777, 0, numeros.length-1));
	}

	public static int busca(int[] numeros, int buscado, int inicio, int fim) {
		if(inicio <= fim){
			int meio = (inicio + fim) / 2;
			if( numeros[meio] == buscado){
				return meio;
			} else if( numeros[meio] > buscado){
				return busca(numeros,buscado,inicio,meio-1);
			} else {
				return busca(numeros,buscado,meio+1,fim);
			}
		} 
		else {
			// nusca chega aqui?
			return buscado;
		}
	}
}
