import java.util.Arrays;

public class fibonacciRecursaoCache {

	public static void main(String[] args){
		int[] cache = new int[10]; // array com as 7 posições com zeros =)
		System.out.println(fibonacci(9 ,cache));
	}

	public static int fibonacci(int index, int[] cache) {

		if(index == 0 || index == 1){
			cache[index] = 1;
			return 1;
		}
		else {
			if(cache[index - 1] == 0 ) {
				//cache[index-1] = fibonacci(index - 1,cache);
				fibonacci(index - 1,cache);
			}
			if(cache[index - 2] == 0 ) {
				//cache[index-2] = fibonacci(index - 2,cache);
				fibonacci(index - 2,cache);
			}
			cache[index] = cache[index-1] + cache[index-2];
			return cache[index];
		}
	}
}
