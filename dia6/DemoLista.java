import java.util.Arrays;
import java.util.ArrayList;

public class DemoLista {

	public static void main(String[] args){
		// lib default
		int qtd = 10_000_000;
		ArrayList lista2 = new ArrayList(qtd);
		for(int i=0; i < qtd; i++){
			lista2.add(i);
			if(i%100_000 == 0){
				System.out.println(i);
			}
		}

		// testa
		ListaComArray lista = new ListaComArray();
		
		lista.adiciona("Ana");
		lista.adiciona("Josefa");
		lista.adiciona("Camila");
		//lista.adiciona("Camila");
		
		lista.adiciona("Gaby", 0);

		//lista.remove("Camila");
		//lista.remove(0);

		//lista.modifica(1,"Marcelo");

		//System.out.println(Arrays.toString(lista.get()));
		//System.out.println(lista.tamanho());
	}

}
