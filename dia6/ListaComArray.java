public class ListaComArray {
	private String[] elementos = new String[100];
	private int qtd = 0;

	public String[] get(){
		return this.elementos;
	}

	public String get(int posicao){
		this.valida(posicao);	
		return this.elementos[posicao];
	}

	public int tamanho(){
		return this.qtd;
	}

	public void altera(int posicao, String elemento){
		this.valida(posicao);
		this.elementos[posicao] = elemento;
	}

	public void remove(String elemento){
		for(int i = 0 ; i < this.qtd; i++ ){
			if(this.elementos[i].equals(elemento)){
				this.remove(i);
				break;
			}
		}
	}

	public void remove(int posicao){
		this.valida(posicao);	
		// puxar os elementos
		for (int i = posicao+1; i < this.qtd ;i++) {
			this.elementos[i-1] = this.elementos[i];
		}
		this.qtd--;
	}

	// método que adiciona elemento no final da lista
	public void adiciona(String elemento){
		aumentaArraySeNecessario();
		this.elementos[this.qtd] = elemento;
		this.qtd++;
	}

	public void adiciona(String elemento, int posicao) {
		this.valida(posicao);

		// deslocar os elementos para frente
		aumentaArraySeNecessario();
		for (int i = this.qtd-1 ; i >= posicao ; i--) {
			this.elementos[i+1] = this.elementos[i];
		}
		this.elementos[posicao] = elemento;
		this.qtd++;
	}

	private void aumentaArraySeNecessario() {
		if(this.qtd == this.elementos.length){
			String[] novo = new String[this.elementos.length*2];
			for(int i = 0; i < this.elementos.length; i++){
				novo[i] = this.elementos[i];
			}
			this.elementos = novo;
		}
	}

	private void valida(int posicao){
		if(posicao >=  this.qtd || posicao < 0) {
			throw new IllegalArgumentException("Posição Inválida");	
		}
	}

}
