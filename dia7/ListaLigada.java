public class ListaLigada {
	private Celula primeira;
	private Celula ultima;
	private int qtd = 0;

	public void remove(int posicao) {
		if(posicao == 0) {
			this.removeComeco();
		} else if (posicao == this.qtd - 1) {
			this.removeFim();
		} else {
			Celula c2 = this.pegaCelula(posicao);

			Celula c1 =  c2.getAnterior();
			Celula c3 =  c2.getProxima();
			c1.setProxima(c3);
			c3.setAnterior(c1);
			this.qtd--;
		}
	
	}

	public void add(int posicao, String elemento) {
		if(posicao == 0) {
			this.adicionaComeco(elemento);
		} else if (posicao == this.qtd) {
			this.adicionaFinal(elemento);
		} else {
			Celula c1 = this.pegaCelula(posicao - 1);
			Celula c2 = new Celula();
			c2.setElemento(elemento);
			Celula c3 = c1.getProxima();

			c1.setProxima(c2);
			c2.setProxima(c3);
		
			c2.setAnterior(c1);
			c3.setAnterior(c2);
			this.qtd++;
		}
	}

	public String get(int posicao) {
		Celula celula = this.pegaCelula(posicao);
		return celula.getElemento();
	}

	private Celula pegaCelula(int posicao){
		if(posicao < this.qtd/2) {
			Celula atual = this.primeira;
			for(int i = 0; i < posicao; i++){
				atual = atual.getProxima();
			}
			return atual;
		} else {
			// começa pelo fim
			Celula atual = this.ultima;
			for(int i = this.qtd-1; i > posicao; i--){
				atual = atual.getAnterior();
			}
			return atual;
		}
	}

	public void adicionaComeco(String elemento) {
		Celula nova = new Celula();
		nova.setElemento(elemento);

		if(this.qtd == 0) {
			this.ultima = nova;
			this.primeira = nova;
		} else {
			nova.setProxima(this.primeira);
			this.primeira.setAnterior(nova);
			this.primeira = nova;
		}
		this.qtd++;
	}

	public void adicionaFinal(String elemento) {

		Celula nova = new Celula();
		nova.setElemento(elemento);

		if(this.qtd == 0) {
			this.ultima = nova;
			this.primeira = nova;
		} else {
			nova.setAnterior(this.ultima); // **********
			this.ultima.setProxima(nova);
			this.ultima = nova;
		}
		this.qtd++;
	}

	public void removeComeco() {
		if(this.qtd == 1) {
			this.ultima = null;
			this.primeira = null;
			this.qtd = 0;
		} else if ( this.qtd > 1 ) {
			this.primeira = this.primeira.getProxima();
			this.qtd--;
		}
	}

	public void removeFim() {
		if(this.qtd == 1) {
			this.ultima = null;
			this.primeira = null;
			this.qtd = 0;
		} else if ( this.qtd > 1 ) {
			this.ultima = this.primeira.getAnterior();
			this.qtd--;
		}
	}

}
