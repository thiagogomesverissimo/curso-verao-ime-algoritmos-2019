public class achaMaior {
	public static void main(String[] args){
		double[] numeros = {1.,2.,5.,6.,8.,8.,1.,3.,4.,0.};
		double maior = achaMaior(numeros);
		System.out.println(maior);
	}

	public static double achaMaior(double[] numeros){

		double maior = numeros[0];
		for (int i = 1; i < numeros.length; i++) {
			if( numeros[i] > maior) {
				maior = numeros[i];
			}
		}
		return maior;
	}
}
