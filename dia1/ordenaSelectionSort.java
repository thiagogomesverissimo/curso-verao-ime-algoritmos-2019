import java.util.Arrays;

// tipo: selection sort
public class ordenaSelectionSort {

	public static void main(String[] args){
		double[] numeros = {10.,2.,5.,6.,8.,3.,1.0,4.,0.};
		//double maior = achaMaior(numeros);
		//System.out.println( achaPosicaoDoMaior(numeros, 0, 3) );
		System.out.println(Arrays.toString(ordena(numeros)));
	}

	public static double[] ordena(double[] numeros) {

		for (int i = numeros.length - 1; i > 0 ; i--){
			int maior = achaPosicaoDoMaior(numeros,0,i);
			double valor_maior = numeros[maior];
			double valor_i = numeros[i];

			numeros[i] = valor_maior;
			numeros[maior] = valor_i;
		}
		return numeros;
	}

	// Pergunta: no método achaPosicaoDoMaior o intervalo começa em zero ou em um?
	public static int achaPosicaoDoMaior(double[] numeros, int inicio, int fim) {

		int maior = inicio;
		for (int i = inicio + 1; i <= fim; i++) {
			if( numeros[i] > numeros[maior] ) {
				maior = i;
			}
		}
		return maior;
	}
}
