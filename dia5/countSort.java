import java.util.Arrays;

public class countSort {

	public static void main(String[] args){
		// testa
		int[] numeros = new int[] {1,4,5,6,7,8,3,2,5,7,8,8,3,9,6,0,7,1};
		System.out.println(Arrays.toString(countSort(numeros, 9)));
	}

	public static int[] countSort(int[] numeros, int max) {
		int[] qtd = new int[max+1];

		for (int i = 0; i < numeros.length; i++ ){
			int numero = numeros[i];
			qtd[numero]++;
		}
		
		int b = 0;
		for ( int i = 0; i < numeros.length; i++){
			while(qtd[b]==0){
				b++;
			}
			numeros[i] = b;
			qtd[b]--;

		}
		return numeros;
	}

}
