import java.util.Arrays;

public class mergeSort {

	public static void main(String[] args){
		// teste intercala
		//double[] numeros = new double[] {0., 3., 6., 7., 10., 1. , 2., 4., 5.};
		//System.out.println(Arrays.toString(intercala(numeros, 0, 4, 8)));

		// testa mergeSort
		double[] numeros = new double[] {2345., 888., 9009.,3., 6., 7., 10., 1. , 2., 4., 5.};
		System.out.println(Arrays.toString(mergeSort(numeros, 0, numeros.length-1)));
	}

	public static double[] mergeSort(double[] numeros, int inicio, int fim) {
		
		if( inicio < fim ) {
			int meio = (inicio + fim)/2;
			mergeSort(numeros, inicio, meio);
			mergeSort(numeros, meio+1, fim);

			return intercala(numeros,inicio,meio,fim);
		}
		return new double[1];
	}

	public static double[] intercala(double[] numeros, int inicio, int meio, int fim) {
		double[] aux = new double[fim - inicio + 1];

		int a = inicio;
		int b = meio + 1;
		int c = 0;

		while( a <= meio && b <=fim ) {
			if( numeros[a] <= numeros[b]){
				aux[c] = numeros[a];
				a++;
			} else {
				aux[c] = numeros[b];
				b++;
			}
			c++;	
		}

		// copia todos da esquerda
		while( a <= meio ) {
			aux[c] = numeros[a];
			a++;
			c++;
		}

		// copia todos da direita
		while( b <= fim ) {
			aux[c] = numeros[b];
			b++;
			c++;
		}
		
		for(int i = 0; i < aux.length; i++){
			numeros[inicio+i] = aux[i];
		}

		return numeros;
	}
}
