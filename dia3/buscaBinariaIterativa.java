import java.util.Arrays;

public class buscaBinariaIterativa {

	public static void main(String[] args){
		// ATENÇÃO: BUSCA PARA NUMEROS ORDENADOS!!!!!!!!!!!!
		int[] numeros = new int[] {1, 3, 4, 20, 23, 48, 67, 100, 777, 10000};
		System.out.println(busca(numeros, 777));
	}

	public static int busca(int[] numeros, int buscado) {
		int inicio = 0;
		int fim = numeros.length -1;
		int meio = (inicio+fim)/2;
		while(inicio <= fim){
			meio = (inicio+fim)/2;
			if(numeros[meio] == buscado) {
				return meio;
			} else if(numeros[meio] < buscado){
				inicio = meio + 1;
			} else {
				fim = meio - 1;
			}
		}
		return -1;
	}
}
