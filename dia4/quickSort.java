import java.util.Arrays;

public class quickSort {

	public static void main(String[] args){
		// testa
		double[] numeros = new double[] {2., 888., 9009.,3., 6., 7., 10., 1. , 2., 4., 5.};
		//System.out.println(separa(numeros, 0, numeros.length-1));

		// teste intercala
		System.out.println(Arrays.toString(quickSort(numeros, 0, numeros.length - 1)));
	}

	public static int separa(double[] numeros, int inicio, int fim) {
		int i = inicio + (int)(Math.random()*(fim-inicio+1));

		//double pivo = numeros[inicio];

		double pivo = numeros[i];
		numeros[i] = numeros[inicio];
		numeros[inicio] = pivo;

		int a = inicio + 1;
		int b = fim;
		while ( a <= b ) {
			if(pivo <= numeros[b]){
				b--;
			} else {
				double aux = numeros[b];
				numeros[b] = numeros[a];
				numeros[a] = aux;
				a++;
			}
		}
		numeros[inicio] = numeros[b];
		numeros[b] = pivo;
		// retorna posição do pivo
		return b;	
	}

	public static double[] quickSort(double[] numeros, int inicio, int fim) {
		
		if( inicio < fim ) {
			// 1. divisão e conquista
			int posicaoDoPivo = separa(numeros,inicio,fim);

			// 2. resolver
			quickSort(numeros, inicio, posicaoDoPivo-1);
			quickSort(numeros, posicaoDoPivo+1,fim);

			return numeros;
		}
		return new double[1];
	}

}
