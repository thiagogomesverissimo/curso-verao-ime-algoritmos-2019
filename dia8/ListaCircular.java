public class ListaCircular {
	private String[] elementos = new String[10];
	private int qtd = 0;
	private int inicio = 0;

	public String get(int posicao) {
		int posicaoReal = (this.inicio+posicao)%this.elementos.length;
		return this.elementos[posicaoReal];
	}

	public void removeInicio() {
		this.inicio = (this.inicio+1) % this.elementos.length;
		this.qtd--;
	}

	public void removeFim() {
		this.qtd--;
	}

	public int tamanho() {
		return this.qtd;
	}

	public void adicionaFim(String elemento) {
		int posicao = (this.inicio + this.qtd) % this.elementos.length;
		this.elementos[posicao] = elemento;
		this.qtd++;
	}

	public void adicionaComeco(String elemento) {
		int posicao = (this.inicio + this.elementos.length - 1) % this.elementos.length;
		System.out.println("----------------");
		System.out.println(posicao);
		System.out.println("---------------");
		this.elementos[posicao] = elemento;
		this.qtd++;
	}
}
