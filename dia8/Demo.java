import java.util.Arrays;

public class Demo {

	public static void main(String[] args){

		// testa
		ListaCircular lista = new ListaCircular();
		
		lista.adicionaComeco("Zeza");
		System.out.println(lista.get(0));

		lista.adicionaFim("Ana");
		System.out.println(lista.get(1));


		System.out.println(lista.tamanho());		
		lista.removeInicio();
		lista.removeFim();
		System.out.println(lista.tamanho());
		//System.out.println(Arrays.toString(lista.get()));
	}

}
